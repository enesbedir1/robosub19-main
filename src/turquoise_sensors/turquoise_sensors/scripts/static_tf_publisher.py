#!/usr/bin/env python
import rospy
import tf
import random
from tf.transformations import quaternion_from_euler
import sys

if __name__ == '__main__':
    _int = random.randint(0, 10000)
    rospy.init_node('turquoise_static_tf_broadcaster_' + str(_int))
    name = sys.argv[1]
    params = rospy.get_param("~" + name)

    roll = params["roll"]
    pitch = params["pitch"]
    yaw = params["yaw"]
    rate_hz = params["rate"]

    x = params["x"]
    y = params["y"]
    z = params["z"]

    child_frame = params["child_frame"]
    parent_frame = params["parent_frame"]

    broadcaster = tf.TransformBroadcaster()
    (qx, qy, qz, qw) = quaternion_from_euler(roll, pitch, yaw)
    rate = rospy.Rate(rate_hz)
    while not rospy.is_shutdown():
        broadcaster.sendTransform((x, y, z),
            (qx, qy, qz, qw),
            rospy.Time.now(),
            child_frame,
            parent_frame
            )
        rate.sleep()
