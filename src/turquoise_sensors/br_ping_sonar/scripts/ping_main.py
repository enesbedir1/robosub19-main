#!/usr/bin/env python3


import rospy
# from brping import Ping1D
from brping.ping1d import Ping1D
from br_ping_sonar.msg import PingSonarStatus
from sensor_msgs.msg import Range



class PingModuleNode(object):
    """docstring for PingModuleNode."""
    def __init__(self):
        self.device = rospy.get_param("~device")
        self.baudrate = rospy.get_param("~baudrate")
        self._rate = rospy.get_param("~rate")
        self.publish_status = rospy.get_param("~publish_status")

        if rospy.has_param("~confidence_threshold"):
            self.confidence_threshold = rospy.get_param("~confidence_threshold")
        else:
            self.confidence_threshold = 0.0

        self.rate = rospy.Rate(self._rate)
        self.ping = Ping1D(self.device, self.baudrate)

        if self.publish_status:
            self.status_pub = rospy.Publisher("status", PingSonarStatus, queue_size=2)
        self.publish = rospy.Publisher("out", Range, queue_size=2)


    def spin(self):
        while not rospy.is_shutdown():
            dist_ = self.ping.get_distance_simple()
            if self.publish_status:
                msg = PingSonarStatus()
                msg.header.stamp = rospy.Time.now()
                msg.pcb_temperature = self.ping.get_pcb_temperature()["pcb_temperature"] / 100.0
                msg.processor_temperature = self.ping.get_processor_temperature()["processor_temperature"] / 100.0

                version_ = self.ping.get_firmware_version()
                msg.firmware_version = str(version_["firmware_version_major"]) + "." + str(version_["firmware_version_minor"])
                msg.device_id = str(self.ping.get_device_id()["device_id"])
                msg.ping_interval = self.ping.get_ping_interval()["ping_interval"]
                msg.speed_of_sound = self.ping.get_speed_of_sound()["speed_of_sound"]
                msg.ping_enable = self.ping.get_ping_enable()["ping_enabled"] == 1
                msg.gain_index = self.ping.get_gain_index()["gain_index"]
                msg.mode_auto = self.ping.get_mode_auto()["mode_auto"] == 1
                msg.range = self.ping.get_range()["scan_length"]
                msg.distance = dist_["distance"] / 1000.0 # convert to m
                msg.confidence = dist_["confidence"]
                self.status_pub.publish(msg)

            # Publish Range msg

            msg = Range()
            msg.header.stamp = rospy.Time.now()
            msg.header.frame_id = "ping_front"
            msg.max_range = 30
            msg.min_range = 0.5
            msg.radiation_type = msg.ULTRASOUND
            if dist_["confidence"] > self.confidence_threshold:
                msg.range = dist_["distance"] / 1000.0 # convert to meters.
            self.publish.publish(msg)

            self.rate.sleep()

if __name__ == "__main__":
    rospy.init_node("ping_module_node")
    node = PingModuleNode()
    node.spin()
