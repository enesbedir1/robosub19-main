#!/usr/bin/env python

import rospy
from turquoise_mavros_plugins.apm_control import MavControlClient


# Example of usage
# BEGIN SCRIPT
rospy.init_node("mavros_command_control", anonymous=False)
cl = MavControlClient()

print "Rate: =>\n", cl.set_rate(100)

# TODO: Implement Mavros State watcher

# END OF SCRIPT
