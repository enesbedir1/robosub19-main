#!/usr/bin/env python
import rospy
from mavros_msgs.msg import OverrideRCIn
from geometry_msgs.msg import Twist

# pinmap of rc controller
# 1 	Pitch
# 2 	Roll
# 3 	Throttle
# 4 	Yaw
# 5 	Forward
# 6 	Lateral
# 7 	Camera Pan
# 8 	Camera Tilt
# 9 	Lights 1 Level
# 10 	Lights 2 Level
# 11 	Video Switch

class ThrusterManager(object):
    """docstring for ThrusterManager."""
    def __init__(self):
        self.pub = rospy.Publisher("output", OverrideRCIn, queue_size=2)
        self.sub = rospy.Subscriber("cmd_vel", Twist, self.callback)
        self.flip = rospy.get_param("~flip")
        self.us_range = rospy.get_param("~pwm_range")
        self.cmd_max = 1.0

        self.pwm_vector = []
        rate_ = rospy.get_param("~rate")
        self.rate = rospy.Rate(rate_)

    def constrain(self, val, min_, max_):
        if val > max_:
            return max_
        elif val < min_:
            return min_
        else:
            return val

    def callback(self, data):

        vector = [0.0 for i in range(8)]

        vector[0] = data.angular.y # Pitch
        vector[1] = data.angular.x # Roll
        vector[2] = data.linear.z  # Throttle
        vector[3] = data.angular.z # Yaw
        vector[4] = data.linear.x  # Surge / Forward
        vector[5] = data.linear.y  # Sway / Lateral

        vector = [vector[i] * self.us_range / self.cmd_max for i in range(8)]
        self.pwm_vector = [1500 - vector[i] if self.flip[i] else 1500 + vector[i] for i in range(8)]
        self.pwm_vector = [self.constrain(self.pwm_vector[i], 1500 - self.us_range, 1500 + self.us_range) for i in range(8)]



    def spin(self):
        while not rospy.is_shutdown():
            rc_msg = OverrideRCIn()
            rc_msg.channels = self.pwm_vector
            self.pub.publish(rc_msg)
            self.rate.sleep()

if __name__ == "__main__":
    rospy.init_node("turquoise_thruster_manager", anonymous=False)

    manager = ThrusterManager()
    manager.spin()
