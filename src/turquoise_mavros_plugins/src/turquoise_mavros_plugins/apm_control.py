#!/usr/bin/env python

import rospy
from mavros_msgs.msg import State
from mavros_msgs.srv import SetMode, StreamRate, StreamRateRequest, CommandHome, CommandHomeRequest, CommandBool, CommandLong, CommandLongRequest
# from mavros_msgs.srv import CommandLongRequest, CommandBoolRequest, CommandTOLRequest, CommandHomeRequest, SetModeRequest, StreamRateRequest, SetModeRequest
# from mavros_msgs.srv import *

class MavControlClient():
    """docstring for MavControlClient."""
    def __init__(self):
        # Set publishers and subscribers
        self.state_sub = rospy.Subscriber("/mavros/state", State, self.state_cb)

        # Set variables
        self.timeout = 1.0
        self.current_mode = ""
        self.current_arm_status = False

        self.scend_rate = 0.5
        self.takeoff_depth = 0.5
        # Start Client
        self.wait_services()
        self.init_services()


    def wait_services(self):
        print "Waiting for Services ...\n"
        rospy.wait_for_service('/mavros/cmd/arming')
        rospy.wait_for_service('/mavros/cmd/command')
        rospy.wait_for_service('/mavros/set_mode')
        rospy.wait_for_service('/mavros/set_stream_rate')


    def init_services(self):
        self.arming_client = rospy.ServiceProxy("/mavros/cmd/arming", CommandBool)
        self.takeoff_client = rospy.ServiceProxy("/mavros/cmd/command", CommandLong)
        self.mode_client = rospy.ServiceProxy("/mavros/set_mode", SetMode)
        self.stream_client = rospy.ServiceProxy("/mavros/set_stream_rate", StreamRate)
        self.homepos_client = rospy.ServiceProxy("/mavros/cmd/set_home", CommandHome)

    def set_rate(self, rate, id=StreamRateRequest.STREAM_ALL):
        print "***--- Sending STREAM RATE Request ---***"
        print ""
        try:
            self.stream_client(stream_id=id, message_rate=rate, on_off=(rate != 0))
            return True
        except rospy.ServiceException as ex:
            print ex
            return False

    def set_mode(self, mode):
        print "***--- Sending MODE CHANCE Request ---***"
        mode = mode.upper()

        resp = self.mode_client(base_mode=0, custom_mode=mode)
        if resp.mode_sent:
            print "Requested Mode Change to", mode
            print "Current Mode is", self.current_mode

        request_succeeded = False
        _time = 0.0
        rate = rospy.Rate(5)
        if self.current_mode != mode:
            while self.current_mode != mode and _time < self.timeout:
                _time += rate.remaining().secs +  rate.remaining().nsecs / 1000000000.0
                print "Mode change requested, no response for:", _time
                rate.sleep()

        request_succeeded = (self.current_mode == mode)
        if request_succeeded:
            print "Request Succeeded, Mode Changed to", mode
        else:
            print "Request has timeout for", self.timeout, "seconds. Check Mode manually"
        print ""
        return request_succeeded

    def set_arming(self, arm):
        print "***--- Sending ARM/DISARM Request ---***"
        resp = self.arming_client(arm)
        if resp.success:
            print "Arming/Disarming Request Sent"
        else:
            print "Failed to Arm/Disarm, REASON:",
            print resp

        request_succeeded = False
        _time = 0.0
        rate = rospy.Rate(5)
        if self.current_arm_status != arm:
            while self.current_arm_status != arm and _time < self.timeout:
                _time += rate.remaining().secs +  rate.remaining().nsecs / 1000000000.0
                print "Arming/Disarming requested, no response for:", _time
                rate.sleep()

        request_succeeded = (self.current_arm_status == arm)
        if request_succeeded:
            print "Request Succeeded, Arming Changed to", arm
        else:
            print "Request has timeout for", self.timeout, "seconds. Check Arm State manually"
        print ""
        return request_succeeded

    def set_takeoff(self, take_off):
        NAV_LAND_LOCAL=23
        NAV_TAKEOFF_LOCAL=24
        cmd = NAV_LAND_LOCAL
        depth = 0
        if take_off:
            cmd = NAV_TAKEOFF_LOCAL
            depth = self.takeoff_depth


        srv = CommandLongRequest()
        srv.command = cmd
        srv.param1 = 0
        srv.param2 = 0
        srv.param3 = self.scend_rate #scend_rate
        srv.param4 = 0
        srv.param5 = 0
        srv.param6 = 0
        srv.param7 = depth

        resp = self.takeoff_client(srv)
        print resp

    def set_home(self, home_pos):
        srv = CommandHomeRequest()
        srv.current_gps = True
        srv.altitude = home_pos["alt"]
        srv.latitude = home_pos["lat"]
        srv.longitude = home_pos["lon"]

        resp = self.homepos_client(srv)
        print resp

    def state_cb(self, data):
        self.current_mode = data.mode
        self.current_arm_status = data.armed




# Example of usage
# BEGIN SCRIPT
# rospy.init_node("mavros_command_control", anonymous=False)
# cl = MavControlClient()

# print "Mode", cl.set_mode("manual")
# print "Rate", cl.set_rate(100)
# print cl.set_home({"alt": 408., "lat": 47.3667, "lon": 8.5500})
# print "Arming", cl.set_arming(True)
# print cl.set_takeoff(True)

# END OF SCRIPT
