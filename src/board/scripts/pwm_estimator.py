import yaml
import json

file = open("t100.json", "r+")
force2pwm = yaml.safe_load(file)
file.close()

def estimate_pwm(force, data):
    uS = int(min(data.items(), key=lambda x: abs(x[1]["newtons"]-force))[0])
    if force >= data[str(uS)]["newtons"] and force < data[str(uS+10)]["newtons"]:
        error = (force - data[str(uS)]["newtons"]) / (data[str(uS+10)]["newtons"] - data[str(uS)]["newtons"])
        return uS + error * 10.0
    elif force >= data[str(uS)]["newtons"] and force < data[str(uS-10)]["newtons"]:
        error = (force - data[str(uS)]["newtons"]) / (data[str(uS-10)]["newtons"] - data[str(uS)]["newtons"])
        return uS + error * 10.0
    elif force <= data[str(uS)]["newtons"] and force > data[str(uS-10)]["newtons"]:
        error = (data[str(uS)]["newtons"] - force) / (-data[str(uS-10)]["newtons"] + data[str(uS)]["newtons"])
        return uS - error * 10.0
    elif force <= data[str(uS)]["newtons"] and force > data[str(uS+10)]["newtons"]:
        error = (data[str(uS)]["newtons"] - force) / (-data[str(uS+10)]["newtons"] + data[str(uS)]["newtons"])
        return uS - error * 10.0
    elif force == data[str(uS)]["newtons"]:
        return uS
    else:
        return 1500
