import rospy
from board.msg import BoardOut

rospy.init_node("board_test")
pub = rospy.Publisher("/turquoise/board/out", BoardOut, queue_size=2)

rate = rospy.Rate(10)
import sys
val = [1500, 1500, 1500, 1500, 1500, 1500, 1500, 1500]
while not rospy.is_shutdown():
    print "SENDING"
    msg = BoardOut()
    msg.message = "motors:" + ",".join([str(i) for i in val])
    pub.publish(msg)
    rate.sleep()
