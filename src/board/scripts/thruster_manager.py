#!/usr/bin/env python
import yaml
import json
import rospy
from geometry_msgs.msg import Twist
from board.msg import BoardOut
import numpy
from uuv_gazebo_ros_plugins_msgs.msg import FloatStamped
import time

# class ThrusterManagerNode():
#     def __init__(self):
#         self.t100_filepath = rospy.get_param("~t100_characterization_path")
#         tam_ = rospy.get_param("~tam")
#         self.tam = numpy.array(tam_)
#         self.gain = rospy.get_param("~gain")
#         self.max_thrust = rospy.get_param("~max_thrust")
#
#         file = open(self.t100_filepath, "r+")
#         self.force2pwm = yaml.safe_load(file)
#         file.close()
#
#         self.sub = rospy.Subscriber("/turquoise/cmd_vel", Twist, self.callback)
#         self.pub = rospy.Publisher("/turquoise/board/out", BoardOut, queue_size=2)
#         self.arming_sub = rospy.Subscriber
#
#     def callback(self, data):
#         linear_x = self.tam[0]
#         linear_y = self.tam[1]
#         linear_z = self.tam[2]
#
#         angular_x = self.tam[3]
#         angular_y = self.tam[4]
#         angular_z = self.tam[5]
#
#         cartesian = data.linear.x * linear_x + data.linear.y * linear_y + data.linear.z * linear_z
#         rotational = data.angular.x * angular_x + data.angular.y * angular_y + data.angular.z * angular_z
#         out = cartesian + rotational
#
#         # Get to newtons
#         out *= self.max_thrust * self.gain
#         # Get pwm
#         pwm_out = [self.estimate_pwm(i, self.force2pwm) for i in out]
#         def constrain(val, _min, _max):
#             if val < _min:
#                 return _min
#             if val > _max:
#                 return _max
#             return val
#
#
#         pwm_out = [int(constrain(i, 1100, 1900)) for i in pwm_out]
#         msg = BoardOut()
#         msg.message = "motors:" + ",".join([str(i) for i in pwm_out])
#         self.pub.publish(msg)
#
#     def estimate_pwm(self, force, data):
#         uS = int(min(data.items(), key=lambda x: abs(x[1]["newtons"]-force))[0])
#         if force >= data[str(uS)]["newtons"] and force < data[str(uS+10)]["newtons"]:
#             error = (force - data[str(uS)]["newtons"]) / (data[str(uS+10)]["newtons"] - data[str(uS)]["newtons"])
#             return uS + error * 10.0
#         elif force >= data[str(uS)]["newtons"] and force < data[str(uS-10)]["newtons"]:
#             error = (force - data[str(uS)]["newtons"]) / (data[str(uS-10)]["newtons"] - data[str(uS)]["newtons"])
#             return uS + error * 10.0
#         elif force <= data[str(uS)]["newtons"] and force > data[str(uS-10)]["newtons"]:
#             error = (data[str(uS)]["newtons"] - force) / (-data[str(uS-10)]["newtons"] + data[str(uS)]["newtons"])
#             return uS - error * 10.0
#         elif force <= data[str(uS)]["newtons"] and force > data[str(uS+10)]["newtons"]:
#             error = (data[str(uS)]["newtons"] - force) / (-data[str(uS+10)]["newtons"] + data[str(uS)]["newtons"])
#             return uS - error * 10.0
#         elif force == data[str(uS)]["newtons"]:
#             return uS
#         else:
#             return 1500
#
#
# if __name__ == "__main__":
#     rospy.init_node("thruster_manager_node")
#     node = ThrusterManagerNode()
#     rospy.spin()

class ThrusterManagerNode():
    def __init__(self):
        self.pwm = [1500 for i in range(8)]
        self.sub = [rospy.Subscriber("/turquoise/thrusters/" + str(i) +  "/input", FloatStamped, self.callback, i) for i in range(8)]
        self.pub = rospy.Publisher("/turquoise/board/out", BoardOut, queue_size=2)
        self.rate = rospy.Rate(100)

    def constrain(self, val, mi, ma):
        if val > ma:
            return ma
        if val < mi:
            return mi
        return val

    def callback(self, data, index):
        self.pwm[index] = int(data.data)

    def spin(self):
        while not rospy.is_shutdown():
            self.pwm = [int(self.constrain(i, 1100, 1900)) for i in self.pwm]
            msg = BoardOut()
            msg.message = "motors:" + ",".join([str(i) for i in self.pwm]) + "\n"
            self.pub.publish(msg)
            self.rate.sleep()


if __name__ == "__main__":
    rospy.init_node("thruster_manager_node")
    time.sleep(2)
    node = ThrusterManagerNode()
    node.spin()
