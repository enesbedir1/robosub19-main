# Start USB Camera


```
roslaunch usb_cam usb_cam-test.launch
```

# Start ORB Slam
Important, read orbslam readme!
For mono:
```
rosrun ORB_SLAM2 ORB_SLAM2/Vocabulary/ORBvoc.txt ORB_SLAM2/Examples/Monocular/TUM1.yaml /camera/image_raw:=/usb_cam/image_raw
```
For Stereo:
```
STEREO ====> rosrun ORB_SLAM2 Stereo Vocabulary/ORBvoc.txt Examples/Stereo/EuRoC.yaml true
```

# Dependencies

## usb_cam 
```
sudo apt-get install ros-kinetic-usb-cam 
```
