#!/usr/bin/env python

# Converts the fluid pressure to pose or range msg by
# subtracting atmosferic pressure !
import rospy
from sensor_msgs.msg import FluidPressure
from sensor_msgs.msg import Range
from geometry_msgs.msg import PoseWithCovarianceStamped

def callback(data):
    global pub, standard_pressure, kPa_per_meter, ned, output_type
    # TODO: Calculate covariance from fluid pressure variance
    depth = (data.fluid_pressure - standard_pressure) / kPa_per_meter

    range_msg = Range()
    range_msg.header.frame_id = data.header.frame_id
    range_msg.header.stamp = data.header.stamp

    pose_msg = PoseWithCovarianceStamped()
    pose_msg.header.frame_id = data.header.frame_id
    pose_msg.header.stamp = data.header.stamp

    if ned:
        range_msg.range = abs(depth)
        pose_msg.pose.pose.position.z = abs(depth)
        pose_msg.pose.pose.orientation.w = 1.0
    else:
        range_msg.range = -abs(depth)
        pose_msg.pose.pose.orientation.w = 1.0
        pose_msg.pose.pose.position.z = -abs(depth)


    if output_type == "both":
        pub[0].publish(pose_msg)
        pub[1].publish(range_msg)
    elif output_type  == "range":
        pub[1].publish(range_msg)
    elif output_type == "pose":
        pub[0].publish(pose_msg)

rospy.init_node("depth_estimator", anonymous=False)

in_top = rospy.get_param("~input_topic")
out_top = rospy.get_param("~output_topic")
output_type = rospy.get_param("~output_type")
standard_pressure = rospy.get_param("~standard_pressure")
kPa_per_meter = rospy.get_param("~kPa_per_meter")
ned = rospy.get_param("~use_ned")
# standardPressure="101.325"
# kPaPerM="9.80638"

pub = []
if output_type in ["both", "range", "pose"]:
    pub = [rospy.Publisher(out_top + "/pose", PoseWithCovarianceStamped, queue_size=2),
        rospy.Publisher(out_top + "/range", Range, queue_size=2)]
else:
    rospy.logerr("Unknown output type: " + output_type)
    exit()

rospy.Subscriber(in_top, FluidPressure, callback)
rospy.spin()
