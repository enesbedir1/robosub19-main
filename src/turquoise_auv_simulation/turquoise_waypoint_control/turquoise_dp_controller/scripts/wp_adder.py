#!/usr/bin/env python

import rospy
import numpy
import uuv_waypoints
from turquoise_controller_msgs.msg import Waypoint as WaypointMessage, WaypointSet as WaypointSetMessage

import geometry_msgs
import std_msgs.msg

waypoint_publisher = rospy.Publisher('wp',WaypointMessage,queue_size=1)

rospy.init_node("waypoint_adder")
i = 0
rate = rospy.Rate(10) # 10hz
print "Waypoint Adder Node"
if __name__ == '__main__':
    if not rospy.is_shutdown():
        for j in range(5):
            wp = WaypointMessage()
            wp.header.stamp = rospy.Time.now()
            wp.header.frame_id = "world"
            wp.point.x = i
            wp.point.y = 10.0
            wp.point.z = -20.0
            wp.max_forward_speed = 0.4
            wp.heading_offset = 0.1
            wp.use_fixed_heading = False
            wp.radius_of_acceptance = 0.1
            waypoint_publisher.publish(wp)
            i += 20
            rate.sleep()
