#!/usr/bin/env python

import rospy
import numpy
import uuv_waypoints
from turquoise_controller_msgs.msg import Waypoint as WaypointMessage, WaypointSet as WaypointSetMessage
from turquoise_controller_msgs.srv import AddWaypointSet
from std_msgs.msg import String, Time

wp_publisher = rospy.Publisher("waypoint_set",WaypointSetMessage,queue_size=1)

rospy.init_node("waypoint_add")
wp_set = uuv_waypoints.WaypointSet()
wp_set_msg = WaypointSetMessage()
def waypoint_callback(data):
    rospy.loginfo('waypoint callback')
    wp_set.add_waypoint_from_msg(data)
    wp_set_msg = wp_set.to_message()
    wp_publisher.publish(wp_set_msg)
    start_time = rospy.Time.now().to_sec()
    start_now = True
    interpolator = rospy.get_param('~interpolator', 'lipb')
    try:
        rospy.wait_for_service('add_waypoint_set', timeout=10)
    except rospy.ROSException:
        raise rospy.ROSException('Service not available! Closing node...')
    try:
        init_wp = rospy.ServiceProxy(
            'add_waypoint_set',
            AddWaypointSet)
    except rospy.ServiceException as e:
        raise rospy.ROSException('Service call failed, error=%s', str(e))

    success = init_wp(wp_set_msg,Time(rospy.Time.from_sec(start_time)),
                      start_now,
                      String(interpolator))
    if success:
      rospy.loginfo('Waypoints file successfully received')
    else:
      rospy.loginfo('Failed to send waypoints')

waypoint_subscriber = rospy.Subscriber("wp",WaypointMessage,waypoint_callback)


if __name__ == '__main__':

    rospy.spin()
