#!/usr/bin/env python
import rospy
from geometry_msgs.msg import PoseStamped, Twist
from tf.transformations import quaternion_from_euler, euler_from_quaternion

from nav_msgs.msg import Odometry
from nav_msgs.msg import Path
from scipy.spatial import distance
import math
from PID.PIDRegulator import PIDRegulator

class PBTrajectoryController(object):
    """Position Based Trajectory Controller."""

    def __init__(self):
        # Set Params
        self.frequency = 70
        self.execution_in_progress = False

        self.ActiveTrajectory = None

        self.pose_cmd = rospy.Publisher("/turquoise/cmd_pose", PoseStamped, queue_size=2)
        self.pose_sub = rospy.Subscriber("/turquoise/pose_gt", Odometry, self.__odom_cb)
        self.waypoint_sub = rospy.Subscriber("/turquoise/waypoint", PoseStamped, self.__wp_sub)
        self.path_pub = rospy.Publisher("/turquoise/path_gen", Path, queue_size=2)

        self.rate = rospy.Rate(self.frequency)
        self.current_pose = None
        self.target_pose = None
        self.points_per_meter = 100
        self.index_shift = 10
        self.ftime = 0

    def closest_node(self, trajectory):
        node = (self.current_pose.pose.pose.position.x, self.current_pose.pose.pose.position.y, self.current_pose.pose.pose.position.z)
        nodes = []

        for n_obj in trajectory:
            nodes.append((n_obj.pose.position.x, n_obj.pose.position.y, n_obj.pose.position.z))

        closest_index = distance.cdist([node], nodes).argmin()

        if closest_index + self.index_shift >= len(trajectory):
            return trajectory[len(trajectory)- 1], len(trajectory)- 1
        else:
            return trajectory[closest_index + self.index_shift], closest_index + self.index_shift

    def pose2vector(self, pose):
        (r, p, y) = euler_from_quaternion((pose.pose.orientation.x, pose.pose.orientation.y,
            pose.pose.orientation.z, pose.pose.orientation.w))

        return [pose.pose.position.x, pose.pose.position.y, pose.pose.position.z, r, p, y]

    def generate_trajectory(self):
        while self.current_pose == None or self.target_pose == None:
            pass

        start = self.pose2vector(self.current_pose.pose)
        target = self.pose2vector(self.target_pose)

        diff = [(target[i] - start[i]) for i in range(len(target))]
        distance = math.sqrt(diff[0] * diff[0] + diff[1] * diff[1] + diff[2] * diff[2])
        iterations = int(distance * self.points_per_meter)

        d_diff = [diff[i] / iterations for i in range(len(diff))]

        trajectory = []

        for i in range(iterations + 1):
            pos = [start[j] + d_diff[j] * i for j in range(len(start))]
            msg = PoseStamped()
            msg.header.stamp = rospy.Time.now()
            msg.header.frame_id = "world"
            msg.pose.position.x = pos[0]
            msg.pose.position.y = pos[1]
            msg.pose.position.z = pos[2]
            (qx, qy, qz, qw) = quaternion_from_euler(pos[3], pos[4], pos[5])
            msg.pose.orientation.x = qx
            msg.pose.orientation.y = qy
            msg.pose.orientation.z = qz
            msg.pose.orientation.w = qw
            trajectory.append(msg)

        return trajectory

    def SetTarget(self, pose_msg):
        self.target_pose = pose_msg

    def Execute(self):
        self.execution_in_progress = True
        self.ActiveTrajectory = self.generate_trajectory()

    def spin(self):
        while not rospy.is_shutdown():
            if self.execution_in_progress and self.ActiveTrajectory != None:
                closest_point, closest_point_index = self.closest_node(self.ActiveTrajectory)
                msg = closest_point
                self.pose_cmd.publish(msg)
                print closest_point
            else:
                pass

            self.rate.sleep()


    def __odom_cb(self, data):
        self.current_pose = data


    def __wp_sub(self, data):
        self.execution_in_progress = False
        self.SetTarget(data)
        self.Execute()






class VBTrajectoryController(object):
    """Velocity Based Trajectory Controller."""

    def __init__(self):
        # Set Params
        self.frequency = 70
        self.execution_in_progress = False

        self.ActiveTrajectory = None

        self.pose_cmd = rospy.Publisher("/turquoise/cmd_vel", Twist, queue_size=2)
        self.pose_sub = rospy.Subscriber("/turquoise/pose_gt", Odometry, self.__odom_cb)
        # self.waypoint_sub = rospy.Subscriber("/turquoise/waypoint", Path, self.__wp_sub)
        self.path_pub = rospy.Publisher("/turquoise/path_gen", Path, queue_size=2)
        self.local_target_pub = rospy.Publisher("/turquoise/local_target", PoseStamped, queue_size=2)

        self.angle_pid = PIDRegulator(1.5, 0, 0, 1.2)
        self.depth_pid = PIDRegulator(1, 0, 0, 2)
        self.surge_pid = PIDRegulator(1.2, 0, 0, 0.175)
        self.roll_pid = PIDRegulator(1.5, 0, 0, 1.2)
        self.pitch_pid = PIDRegulator(1.5, 0, 0, 1.2)

        self.rate = rospy.Rate(self.frequency)
        self.first_time = True
        self.current_pose = None
        self.start_pos = None
        self.target_pose = None
        self.points_per_meter = 100
        self.index_shift = 30
        self.threshold_radius = 0.1
        self.last_closest_index = -1
        self.enabled = True
        self.current_position_index = 0
        self.Finished = False
        self.total_points_index = 0


    def closest_node(self, trajectory):
        node = (self.current_pose.pose.pose.position.x, self.current_pose.pose.pose.position.y, self.current_pose.pose.pose.position.z)
        nodes = []

        for n_obj in trajectory:
            nodes.append((n_obj.pose.position.x, n_obj.pose.position.y, n_obj.pose.position.z))

        closest_index = distance.cdist([node], nodes).argmin()
        if closest_index < self.last_closest_index:
            closest_index = self.last_closest_index

        self.last_closest_index = closest_index

        if closest_index + self.index_shift >= len(trajectory):
            return trajectory[len(trajectory)- 1], len(trajectory)- 1, closest_index
        else:
            return trajectory[closest_index + self.index_shift], closest_index + self.index_shift, closest_index

    def get_ypr(self, _or):
        return euler_from_quaternion((_or.x, _or.y, _or.z, _or.w))

    def pose2vector(self, pose):
        (r, p, y) = euler_from_quaternion((pose.pose.orientation.x, pose.pose.orientation.y,
            pose.pose.orientation.z, pose.pose.orientation.w))

        return [pose.pose.position.x, pose.pose.position.y, pose.pose.position.z, r, p, y]

    def generate_spline_trajectory(self):
        while self.current_pose == None or self.target_pose == None:
            pass
        trajectory = []
        poses = [self.current_pose.pose] + self.target_pose
        for i in range(len(poses) - 1):
            start = self.pose2vector(poses[i])
            target = self.pose2vector(poses[i+1])

            def B(t, (x0,y0,z0), (x1,y1,z1), (x2,y2,z2), (x3,y3,z3)):
                return [
                    math.pow((1-t), 3) * x0 + 3 * math.pow((1-t), 2) * t * x1 +
                         3 * math.pow((1-t), 1) * t * t * x2 + math.pow(t, 3) * x3,
                    math.pow((1-t), 3) * y0 + 3 * math.pow((1-t), 2) * t * y1 +
                         3 * math.pow((1-t), 1) * t * t * y2 + math.pow(t, 3) * y3,
                    math.pow((1-t), 3) * z0 + 3 * math.pow((1-t), 2) * t * z1 +
                         3 * math.pow((1-t), 1) * t * t * z2 + math.pow(t, 3) * z3,
                ]

            start_roll = start[3]
            start_pitch = start[4]
            start_yaw = start[5]

            target_roll = start[3]
            target_pitch = start[4]
            target_yaw = start[5]

            K = 1
            control_1_x = -math.cos(start_yaw) * math.sin(start_pitch) * math.sin(start_roll) - math.sin(start_yaw) * math.cos(start_roll) * K
            control_1_y = -math.sin(start_yaw) * math.sin(start_pitch) * math.sin(start_roll) + math.cos(start_yaw) * math.cos(start_roll) * K
            control_1_z =  math.cos(start_pitch) * math.sin(start_roll) * K


            control_2_x = math.cos(target_yaw) * math.sin(target_pitch) * math.sin(target_roll) - math.sin(target_yaw) * math.cos(target_roll) * K
            control_2_y = math.sin(target_yaw) * math.sin(target_pitch) * math.sin(target_roll) + math.cos(target_yaw) * math.cos(target_roll) * K
            control_2_z = -math.cos(target_pitch) * math.sin(target_roll) * K


            bezier_x0 = (start[0], start[1], start[2])
            bezier_x1 = (control_1_x, control_1_y, control_1_z)
            bezier_x2 = (control_2_x, control_2_y, control_2_z)
            bezier_x3 = (target[0], target[1], target[2])


            bezier_distance = 0

            def get_dist(p1, p2):
                dist = [abs(p1[x] - p2[x]) for x in range(3)]
                return math.sqrt(math.pow(dist[0], 2) + math.pow(dist[1], 2) + math.pow(dist[2], 2))

            for it in range(1000):
                t_time = it / 1000.0
                # sum all distance chunks
                bezier_distance += get_dist(B(t_time, bezier_x0, bezier_x1, bezier_x2, bezier_x3), B(t_time + 0.001, bezier_x0, bezier_x1, bezier_x2, bezier_x3))

            iterations = int(bezier_distance * self.points_per_meter)
            for k in range(iterations + 1):
                pos = B(k / iterations, bezier_x0, bezier_x1, bezier_x2, bezier_x3)

                msg = PoseStamped()
                msg.header.stamp = rospy.Time.now()
                msg.header.frame_id = "world"
                msg.pose.position.x = pos[0]
                msg.pose.position.y = pos[1]
                msg.pose.position.z = pos[2]
                trajectory.append(msg)
        return trajectory


    def generate_trajectory(self):
        while self.current_pose == None or self.target_pose == None:
            pass
        trajectory = []
        poses = [self.current_pose.pose] + self.target_pose
        for i in range(len(poses) - 1):

            start = self.pose2vector(poses[i])
            target = self.pose2vector(poses[i+1])

            diff = [(target[i] - start[i]) for i in range(len(target))]
            distance = math.sqrt(diff[0] * diff[0] + diff[1] * diff[1] + diff[2] * diff[2])
            iterations = int(distance * self.points_per_meter)

            d_diff = [diff[i] / iterations for i in range(len(diff))]


            for k in range(iterations + 1):
                pos = [start[j] + d_diff[j] * k for j in range(len(start))]
                msg = PoseStamped()
                msg.header.stamp = rospy.Time.now()
                msg.header.frame_id = "world"
                msg.pose.position.x = pos[0]
                msg.pose.position.y = pos[1]
                msg.pose.position.z = pos[2]
                (qx, qy, qz, qw) = quaternion_from_euler(pos[3], pos[4], pos[5])
                msg.pose.orientation.x = qx
                msg.pose.orientation.y = qy
                msg.pose.orientation.z = qz
                msg.pose.orientation.w = qw
                trajectory.append(msg)
        return trajectory

    def constrain(self, val, min_, max_):
        if val> max_:
            return max_
        elif val < min_:
            return min_
        return val

    def SetTarget(self, pose_msg):
        self.target_pose = pose_msg

    def Execute(self):
        self.execution_in_progress = True
        self.ActiveTrajectory = self.generate_trajectory()
        # self.ActiveTrajectory = self.generate_spline_trajectory()

    def get_yaw_angle(self, pose, t_pose):
        dy = t_pose.pose.position.y - pose.pose.position.y
        dx = t_pose.pose.position.x - pose.pose.position.x
        return math.atan2(dy, dx)


    def __get_vector(self, t_pose, c_pose):
        return

    def spin_once(self):
        if self.execution_in_progress and self.ActiveTrajectory != None:
            local_target, closest_point_index, real_index = self.closest_node(self.ActiveTrajectory)
            self.current_position_index = real_index
            self.total_points_index = len(self.ActiveTrajectory)

            (roll, pitch, yaw) = self.get_ypr(self.current_pose.pose.pose.orientation)

            t_yaw = self.get_yaw_angle(self.current_pose.pose, local_target)

            angle_err = t_yaw - yaw
            yaw_cmd = self.angle_pid.regulate(angle_err, 1.0 / self.frequency)


            x_err = local_target.pose.position.x - self.current_pose.pose.pose.position.x
            y_err = local_target.pose.position.y - self.current_pose.pose.pose.position.y
            distance = math.sqrt(x_err*x_err + y_err*y_err)
            surge_cmd = self.surge_pid.regulate(distance, 1.0 / self.frequency)


            # TODO: constrain to speed control.
            # surge_cmd = self.constrain(surge_cmd, -0.1*self.surge_pid.sat, 0.1*self.surge_pid.sat)

            z_err = local_target.pose.position.z - self.current_pose.pose.pose.position.z
            z_cmd = self.depth_pid.regulate(z_err, 1.0 / self.frequency)


            roll_err = -roll
            roll_cmd = self.roll_pid.regulate(roll_err, 1.0 / self.frequency)

            pitch_err = -pitch
            pitch_cmd = self.roll_pid.regulate(pitch_err, 1.0 / self.frequency)


            if closest_point_index == len(self.ActiveTrajectory) - 1 and distance <= self.threshold_radius:
                # REACHED
                # rospy.loginfo("Reached Destination")

                msg = Twist()
                self.pose_cmd.publish(msg)
                self.Finished = True


                # start position control here !
            else:

                # Control process
                msg = Twist()
                msg.angular.z = yaw_cmd
                msg.linear.z = z_cmd
                msg.linear.x = surge_cmd
                msg.angular.x = roll_cmd
                msg.angular.y = pitch_cmd
                self.pose_cmd.publish(msg)

                # Visualization
                msg = Path()
                msg.header.frame_id = "world"
                msg.poses = self.ActiveTrajectory
                self.path_pub.publish(msg)


                self.local_target_pub.publish(local_target)
        else:
            # ignore me ?
            pass

        self.rate.sleep()


    def __odom_cb(self, data):
        self.current_pose = data
        if self.first_time:
            self.start_pos = data
            self.first_time = False

    def __wp_sub(self, data):
        self.execution_in_progress = False
        self.SetTarget(data.poses)
        self.Execute()

    def kill(self):
        self.enabled = False

    def CancelExecution(self):
        self.execution_in_progress = False
        for i in range(5):
            msg = Twist()
            self.pose_cmd.publish(msg)

# TODO: FOR VELOCITY BASED TRAJECTORY CONTROLLER, IMPLEMENT POSITION HOLD NODE, TO BE ACTIVATED WHEN VBTC IS DISABLED

if __name__ == "__main__":
    rospy.init_node("turquoise_commander", anonymous=False)
    cmd = VBTrajectoryController()
    cmd.spin()
