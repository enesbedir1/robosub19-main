#! /usr/bin/env python
import rospy

import actionlib
import turquoise_trajectory_controller.msg as action
from nav_msgs.msg import Path
from geometry_msgs.msg import PoseStamped
from tf.transformations import quaternion_from_euler


class TrajectoryActionClient(object):
    """docstring for TrajectoryActionClient."""
    def __init__(self):
        super(TrajectoryActionClient, self).__init__()
        self.client = actionlib.SimpleActionClient('trajectory_action_server', action.SendTrajectoryAction)
        self.client.wait_for_server()
        self.client.feedback_cb = self.callback
        

    def spin(self):
        goal = self.create_goal()
        self.client.send_goal(goal)
        self.client.wait_for_result()
        return self.client.get_result()

    def create_goal(self):
        poses = []
        for i in range(5):
            perc = float(i) / 5.0
            x_width = 10.0
            y_width = 10.0
            z_width = 3.0

            z = perc * z_width + 1
            x =  perc * x_width
            y = (perc * perc) * y_width

            msg = PoseStamped()
            msg.header.stamp = rospy.Time.now()
            msg.header.frame_id = "world"
            msg.pose.position.x = x
            msg.pose.position.y = y
            msg.pose.position.z = -z

            (qx, qy, qz, qw) = quaternion_from_euler(30, 0, 0)
            msg.pose.orientation.x = qx
            msg.pose.orientation.y = qy
            msg.pose.orientation.z = qz
            msg.pose.orientation.w = qw
            poses.append(msg)

        path = Path()
        path.header.stamp = rospy.Time.now()
        path.header.frame_id = "world"
        path.poses = poses

        goal = action.SendTrajectoryGoal()
        goal.path = path
        return goal

    def callback(self, data):
        print data



if __name__ == '__main__':
    client = None
    try:
        rospy.init_node('client_test')
        client = TrajectoryActionClient()
        print client.spin()
    except rospy.ROSInterruptException or KeyboardInterrupt:
        print "program interrupted before completion"






#
