#!/usr/bin/env python
import rospy
from geometry_msgs.msg import Twist


rospy.init_node("pose_control_mux")
pub_obj = rospy.Publisher("/turquoise/cmd_vel", Twist, queue_size=10)

pos_control_msg = Twist()


def test_cb(msg):
    global pos_control_msg
    pos_control_msg = msg
    print msg

sub_obj = rospy.Subscriber("/turquoise/orient_lock_cmd_vel", Twist, test_cb)



rate = rospy.Rate(50)
while not rospy.is_shutdown():

    final_msg = Twist()
    final_msg = pos_control_msg
    final_msg.linear.x = 0.2
    pub_obj.publish(final_msg)

    rate.sleep()


