#!/usr/bin/env python

import rospy
from geometry_msgs.msg import Wrench, Twist
from uuv_gazebo_ros_plugins_msgs.msg import FloatStamped


rospy.init_node("test_contorller", anonymous=False)


# pub = rospy.Publisher("/turquoise/thruster_manager/input", Wrench, queue_size=10)
pub = []
count = 8
for i in range(0, count):
    pub.append(rospy.Publisher("/turquoise/thrusters/" + str(i) + "/input", FloatStamped, queue_size=10))


inputs = [100, 0, 0, 0, 0, 0, 0, 0]

rate = rospy.Rate(20)
while not rospy.is_shutdown():
    for i in range(0, count):
        msg = FloatStamped()
        msg.header.stamp = rospy.Time.now()
        msg.header.frame_id = "turquoise/thruster_" + str(i)
        msg.data = inputs[i]
        pub[i].publish(msg)
    rate.sleep()
