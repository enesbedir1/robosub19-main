#!/usr/bin/env python
import rospy
from geometry_msgs.msg import PoseStamped
rospy.init_node("turquoise_commander", anonymous=False)
from tf.transformations import quaternion_from_euler

from nav_msgs.msg import Odometry
from nav_msgs.msg import Path
from scipy.spatial import distance
import math


# get closest node
def __closest_node(x, y, z, nodes_obj, _index_shift):
    node = (x, y, z)
    nodes = []
    for n_obj in nodes_obj:
        nodes.append((n_obj[0], n_obj[1], n_obj[2]))
    closest_index = distance.cdist([node], nodes).argmin()
    if closest_index + _index_shift >= len(nodes_obj):
        return nodes_obj[len(nodes_obj)- 1], len(nodes_obj)- 1
    else:
        return nodes_obj[closest_index + _index_shift], closest_index + _index_shift


# get closest node
def closest_node(node_obj, nodes_obj, _index_shift):
    node = (node_obj.pose.pose.position.x, node_obj.pose.pose.position.y, node_obj.pose.pose.position.z)
    nodes = []
    for n_obj in nodes_obj:
        nodes.append((n_obj.pose.position.x, n_obj.pose.position.y, n_obj.pose.position.z))
    closest_index = distance.cdist([node], nodes).argmin()
    if closest_index + _index_shift >= len(nodes_obj):
        return nodes_obj[len(nodes_obj)- 1], len(nodes_obj)- 1
    else:
        return nodes_obj[closest_index + _index_shift], closest_index + _index_shift

class ControlCommander(object):
    """docstring for ControlCommander."""
    def __init__(self):

        # Set Params
        self.frequency = 50
        self.initiated = True

        self.pose_desired = []
        self.orientation_desired = []


        self.pose_cmd = rospy.Publisher("/turquoise/cmd_pose", PoseStamped, queue_size=2)
        self.pose_sub = rospy.Subscriber("/turquoise/pose_gt", Odometry, self.odom_cb)
        self.path_pub = rospy.Publisher("/turquoise/path_gen", Path, queue_size=2)
        self.rate = rospy.Rate(self.frequency)
        self.last_pose = None
        self.target_point = [30, -20, -1]


    def odom_cb(self, data):
        self.last_pose = data

    def generate_trajectory(self):
        while self.last_pose == None:
            pass

        self.points_per_meter = 50


        self.start_pos = [ self.last_pose.pose.pose.position.x, self.last_pose.pose.pose.position.y,
            self.last_pose.pose.pose.position.z, self.last_pose.pose.pose.orientation.x, self.last_pose.pose.pose.orientation.y,
            self.last_pose.pose.pose.orientation.z, self.last_pose.pose.pose.orientation.w ]


        self.target_pose = [ self.target_point[0], self.target_point[1],
            self.target_point[2], quaternion_from_euler(0, 0, 0)[0], quaternion_from_euler(0, 0, 0)[1],
            quaternion_from_euler(0, 0, 0)[2], quaternion_from_euler(0, 0, 0)[3] ]

        diff = [(self.target_pose[i] - self.start_pos[i]) for i in range(len(self.target_pose))]

        distance = math.sqrt(diff[0] * diff[0] + diff[1] * diff[1] + diff[2] * diff[2])
        self.iterations = int(distance * self.points_per_meter)

        d_diff = [(self.target_pose[i] - self.start_pos[i]) / self.iterations for i in range(len(self.target_pose))]

        self.trajectory = []

        for i in range(self.iterations):
            pos = [self.start_pos[j] + d_diff[j] * i for j in range(len(self.start_pos))]
            msg = PoseStamped()
            msg.header.stamp = rospy.Time.now()
            msg.header.frame_id = "world"
            msg.pose.position.x = pos[0]
            msg.pose.position.y = pos[1]
            msg.pose.position.z = pos[2]
            msg.pose.orientation.x = pos[3]
            msg.pose.orientation.y = pos[4]
            msg.pose.orientation.z = pos[5]
            msg.pose.orientation.w = pos[6]
            self.trajectory.append(msg)

        self.msg = Path()
        self.msg.header.stamp = rospy.Time.now()
        self.msg.header.frame_id = "world"
        self.msg.poses = self.trajectory




    def spin(self):
        while self.initiated and (not rospy.is_shutdown()):
            closest_point, closest_point_index = closest_node(self.last_pose, self.trajectory, 10)
            # print closest_point
            msg = closest_point
            # msg.header.stamp = rospy.Time.now()
            # msg.pose.position.x = 25
            # msg.pose.position.y = -15
            # msg.pose.position.z = -2
            #
            # yaw = 0
            # pitch = 0
            # roll = 0
            # (qx, qy, qz, qw) = quaternion_from_euler(roll, pitch, yaw)
            # msg.pose.orientation.x = qx
            # msg.pose.orientation.y = qy
            # msg.pose.orientation.z = qz
            # msg.pose.orientation.w = qw

            self.pose_cmd.publish(msg)
            self.path_pub.publish(self.msg)
            self.rate.sleep()

cmd = ControlCommander()
cmd.generate_trajectory()
cmd.spin()
